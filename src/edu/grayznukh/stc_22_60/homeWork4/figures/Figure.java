package edu.grayznukh.stc_22_60.homeWork4.figures;

public abstract class Figure {
    protected double x;
    protected double y;
    protected double width;
    protected double height;


    public abstract double getPerimeter();

    public abstract double getArea();

}

package edu.grayznukh.stc_22_60.homeWork4.figures;

/**
 * Представление Эллипса.
 *
 * @author <a href="mailto:nighteagle89@yandex.ru">Антон Грязных</a>
 */
public class Ellipse extends Figure {
    public Ellipse(double x, double y, double majorAxis, double minorAxis) {
        this.x = x;
        this.y = y;
        this.width = majorAxis;
        this.height = minorAxis;
    }

    public Ellipse() {
    }

    @Override
    public String toString() {
        return "Элипса " +
                "x=" + x +
                ", y=" + y;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * Math.sqrt(Math.pow(width / 2, 2) + Math.pow(height / 2, 2) / 2);
    }

    public double getArea() {
        return Math.PI * width / 2 * height / 2;
    }

}

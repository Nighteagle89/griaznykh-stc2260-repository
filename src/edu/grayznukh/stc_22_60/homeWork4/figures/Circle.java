package edu.grayznukh.stc_22_60.homeWork4.figures;

/**
 * Представление Круга.
 *
 * @author <a href="mailto:nighteagle89@yandex.ru">Антон Грязных</a>
 */
public class Circle extends Ellipse implements Movable {
    public Circle(double x, double y, double radius) {
        this.x = x;
        this.y = y;
        this.height = 2 * radius;
        this.width = 2 * radius;
    }

    @Override
    public String toString() {
        return "Круга " +
                "x=" + x +
                ", y=" + y;
    }

    @Override
    public double getPerimeter() {
        return Math.PI * height;
    }

    public double getArea() {
        return Math.PI * Math.pow(height / 2, 2);
    }


    @Override
    public void move(int x, int y) {
        this.x += x;
        this.y += y;

    }

}

package edu.grayznukh.stc_22_60.homeWork4.figures;

/**
 * Представление Квадрата.
 *
 * @author <a href="mailto:nighteagle89@yandex.ru">Антон Грязных</a>
 */
public class Square extends Rectangle implements Movable {
    public Square(double x, double y, double width, double height) {
        super(x, y, width, height);
    }

    @Override
    public String toString() {
        return "Квадрата " +
                "x=" + x +
                ", y=" + y;
    }

    @Override
    public double getPerimeter() {
        return super.getPerimeter();
    }

    public double getArea() {
        return super.getArea();
    }

    @Override
    public void move(int x, int y) {
        this.x += x;
        this.y += y;
    }
}

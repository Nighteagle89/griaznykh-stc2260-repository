package edu.grayznukh.stc_22_60.homeWork4.figures;

public interface Movable {
    void move(int x, int y);
}

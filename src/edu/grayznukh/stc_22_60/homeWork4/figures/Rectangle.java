package edu.grayznukh.stc_22_60.homeWork4.figures;

/**
 * Представление Прямоугольника.
 *
 * @author <a href="mailto:nighteagle89@yandex.ru">Антон Грязных</a>
 */
public class Rectangle extends Figure {

    public Rectangle(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return "Прямоугольника " +
                "x=" + x +
                ", y=" + y;
    }

    @Override
    public double getPerimeter() {
        return 2 * (width + height);
    }

    @Override
    public double getArea() {
        return width * height;
    }

}

package edu.grayznukh.stc_22_60.homeWork4;

import edu.grayznukh.stc_22_60.homeWork4.figures.*;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(-4, 2, 2);
        Rectangle rectangle = new Rectangle(1.75, 2.5, 2.5, 1);
        Ellipse ellipse = new Ellipse(-3, 1, 3, 2.4);
        Square square = new Square(-5, -3, 2, 2);


        Figure[] figures = new Figure[]{circle, rectangle, ellipse, square};
        for (Figure figure : figures) {
            System.out.println("Периметр " + figure.getClass().getSimpleName() + ": " + figure.getPerimeter());
            System.out.println("Площадь " + figure.getClass().getSimpleName() + ": " + figure.getArea());
            System.out.println();
         
        }


        Random random = new Random();

        Movable[] movables = new Movable[]{circle, square};
        for (Movable movable : movables) {
            System.out.println("Координаты центра " + movable.toString());
            int toMoveX = random.nextInt(-2, 3);
            int toMoveY = random.nextInt(-2, 3);
            movable.move(toMoveX, toMoveY);
            System.out.println("Координаты центра после переноса " + movable);

        }

    }
}



package edu.grayznukh.stc_22_60.HomeWork6;

import java.util.Objects;
/**
 * @author <a href="mailto:nighteagle89@yandex.ru">Антон Грязных</a>
 */

public class Human {

    public static final String BD = """
            %s %s %s
            Паспорт:
            Серия: %s Номер: %s
            Город %s, ул. %s, дом %s, квартира %s
                            
            """;
    private final String name;
    private final String lastName;
    private final String patronymic;
    private final String city;
    private final String street;
    private final String house;
    private final String flat;
    private final String numberPassport;


    public Human(String name,
                 String lastName,
                 String patronymic,
                 String city,
                 String street,
                 String house,
                 String flat,
                 String numberPassport
    ) {
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.numberPassport = numberPassport;

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return numberPassport.equals(human.numberPassport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberPassport);
    }


    public String toString() {
        return String.format(BD,
                this.lastName,
                this.name,
                this.patronymic,
                this.numberPassport.substring(0, 5), this.numberPassport.substring(6),
                this.city, this.street, this.house, this.flat);

    }

}




package edu.grayznukh.stc_22_60.HomeWork6;

public class Main {
    public static void main(String[] args) {
        Human vasya = new Human("Вася",
                "Пупкин",
                "Варфаламеевич",
                "Джава",
                "Программистов",
                "15",
                "54",
                "98 22 897643");

        Human ivan = new Human("Иван",
                "Васильевич",
                "Сафронов",
                "Москва",
                "Ленина",
                "1",
                "9",
                "95 13 650215");

        Human ilya = new Human("Илья",
                "Никитин",
                "Станиславович",
                "Казань",
                "Петропавловская",
                "14",
                "52",
                "98 22 897643");
        System.out.println(vasya);
        System.out.println(ivan);
        System.out.println(ilya);
        System.out.println("Сравнение серий и номеров паспортов Василия и Ивана: " + vasya.equals(ivan));
        System.out.println("Сравнение серий и номеров паспортов Василия и Ильи: " + vasya.equals(ilya));

    }
}

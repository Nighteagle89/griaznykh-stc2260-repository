package edu.grayznukh.stc_22_60.attestationWork.service.repository;

import edu.grayznukh.stc_22_60.attestationWork.service.model.User;

import java.io.IOException;

public interface UsersRepositoryFile {
    User findById(int id) throws IOException;

    void create(String firstName,
                String lastName,
                int age,
                boolean employed);

    void update(User user);

    void delete(int id);
}

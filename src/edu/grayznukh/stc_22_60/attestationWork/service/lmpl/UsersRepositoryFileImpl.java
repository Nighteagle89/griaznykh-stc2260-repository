package edu.grayznukh.stc_22_60.attestationWork.service.lmpl;

import edu.grayznukh.stc_22_60.attestationWork.service.model.User;
import edu.grayznukh.stc_22_60.attestationWork.service.repository.UsersRepositoryFile;
import edu.grayznukh.stc_22_60.attestationWork.service.utils.UserUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static edu.grayznukh.stc_22_60.attestationWork.service.utils.UserUtils.FILE;

public class UsersRepositoryFileImpl implements UsersRepositoryFile {

    @Override
    public User findById(int id) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(FILE));
        String line = reader.readLine();

        while (line != null) {
            String[] info = line.split("\\|");

            if (id == Integer.parseInt(info[0])) {
                return UserUtils.convertToUser(line);
            }
            line = reader.readLine();
        }
        reader.close();
        return null;
    }

    @Override
    public void create(String firstName, String lastName, int age, boolean employed) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(FILE, true))) {
            User user = new User(firstName, lastName, age, employed);
            writer.write(UserUtils.convertToString(user));
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(User user) {
        try (BufferedReader reader = new BufferedReader(new FileReader(FILE))) {
            String line = reader.readLine();
            List<User> userList = new ArrayList<>();

            while (line != null) {
                String[] info = line.split("\\|");

                if (user.getId() == Integer.parseInt(info[0])) {
                    long count = userList.stream()
                            .filter(copiedUser -> copiedUser.getId() == Integer.parseInt(info[0]))
                            .count();
                    if (count == 0) {
                        userList.add(user);
                    }
                } else {
                    userList.add(UserUtils.convertToUser(line));
                }
                line = reader.readLine();
            }
            reader.close();

            List<String> userStrings = userList.stream()
                    .map(UserUtils::convertToString)
                    .toList();

            BufferedWriter writer = new BufferedWriter(new FileWriter(FILE));

            for (String userString : userStrings) {
                writer.write(userString);
            }
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(int id) {
        try (BufferedReader reader = new BufferedReader(new FileReader(FILE))) {
            String line = reader.readLine();
            List<String> userList = new ArrayList<>();

            while (line != null) {
                String[] info = line.split("\\|");

                if (id != Integer.parseInt(info[0])) {
                    userList.add(line + "\n");
                }
                line = reader.readLine();
            }
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(FILE));

            for (String userString : userList) {
                writer.write(userString);
            }
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

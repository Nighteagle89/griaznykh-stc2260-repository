package edu.grayznukh.stc_22_60.attestationWork.service.utils;

import edu.grayznukh.stc_22_60.attestationWork.service.model.User;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;

public class UserUtils {
    public static final String FILE = "src/edu/grayznukh/stc_22_60/attestationWork/resources/users.txt";

    public static User convertToUser(String string) {
        StringTokenizer stringTokenizer = new StringTokenizer(string, "|");
        User user = new User(Integer.parseInt(stringTokenizer.nextToken()),
                stringTokenizer.nextToken(),
                stringTokenizer.nextToken(),
                Integer.parseInt(stringTokenizer.nextToken()),
                Boolean.parseBoolean(stringTokenizer.nextToken()));
        return user;
    }

    public static String convertToString(User user) {
        String stringBuilder = user.getId() +
                               "|" +
                               user.getFirstName() +
                               "|" +
                               user.getLastName() +
                               "|" +
                               user.getAge() +
                               "|" +
                               user.isEmployed() +
                               "\n";
        return stringBuilder;
    }

    public static void initCountId() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(FILE));
            String line = reader.readLine();
            String[] strings;
            int count = 0;

            while (line != null) {
                strings = line.split("\\|");
                count = Integer.parseInt(strings[0]);
                line = reader.readLine();
            }

            if (count != -1) {
                User.setIdCount(count + 1);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

package edu.grayznukh.stc_22_60.attestationWork.service.model;

public class User {
    private static int ID_COUNT = 0;
    private final int id;
    private final String firstName;
    private final String lastName;
    private final int age;
    private final boolean employed;

    public User(String firstName, String lastName, int age, boolean employed) {
        this.id = ID_COUNT;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.employed = employed;
        ID_COUNT++;
    }

    public User(int id, String firstName, String lastName, int age, boolean employed) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.employed = employed;
    }

    public static void setIdCount(int idCount) {
        ID_COUNT = idCount;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isEmployed() {
        return employed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (age != user.age) return false;
        if (employed != user.employed) return false;
        if (!firstName.equals(user.firstName)) return false;
        return lastName.equals(user.lastName);
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + age;
        result = 31 * result + (employed ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s | %s | %s | %s | %s |", id, firstName, lastName, age, employed);

    }
}

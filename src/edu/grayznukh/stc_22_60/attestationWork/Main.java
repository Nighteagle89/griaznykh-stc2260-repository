package edu.grayznukh.stc_22_60.attestationWork;

import edu.grayznukh.stc_22_60.attestationWork.service.lmpl.UsersRepositoryFileImpl;
import edu.grayznukh.stc_22_60.attestationWork.service.model.User;
import edu.grayznukh.stc_22_60.attestationWork.service.repository.UsersRepositoryFile;
import edu.grayznukh.stc_22_60.attestationWork.service.utils.UserUtils;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import static edu.grayznukh.stc_22_60.attestationWork.service.utils.UserUtils.FILE;

public class Main {

    public static void main(String[] args) throws IOException {
        File file = new File(FILE);

        UsersRepositoryFile userService = new UsersRepositoryFileImpl();

        Scanner scanner = new Scanner(System.in);

        if (file.createNewFile()) {
            System.out.println("Successful");
        }
        UserUtils.initCountId();
        System.out.println("Введите имя: ");
        String firstName = scanner.nextLine();
        System.out.println("Введите фамилию: ");
        String lastName = scanner.nextLine();
        System.out.println("Введите возраст: ");
        int age = scanner.nextInt();
        System.out.println(" Есть работа?: ");
        boolean employed = scanner.nextBoolean();

        userService.create(firstName, lastName, age, employed);
        User user = userService.findById(2);
        System.out.println(user);
        userService.update(new User(4, "Ивашка", "Мищенко", 25, true));
        userService.delete(5);

    }
}

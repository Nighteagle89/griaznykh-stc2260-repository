package edu.grayznukh.stc_22_60.homeWork5;


/**
 * @author <a href="mailto:nighteagle89@yandex.ru">Антон Грязных</a>
 */
public class Utils {
    private Utils() {
        throw new UnsupportedOperationException("Utility classes should not be instantiated");
    }

    public static boolean isEven(int x) {
        return x % 2 == 0;
    }
}

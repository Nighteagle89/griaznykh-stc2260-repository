package edu.grayznukh.stc_22_60.homeWork5;

@FunctionalInterface
public interface ByCondition {
    boolean isOk(int number);
}

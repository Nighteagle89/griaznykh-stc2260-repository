package edu.grayznukh.stc_22_60.homeWork5;


import java.util.Arrays;

/**
 * @author <a href="mailto:nighteagle89@yandex.ru">Антон Грязных</a>
 */

public class Sequence {
    public static final int BASE = 10;

    public static int[] filter(int[] array, ByCondition byCondition) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (byCondition.isOk(array[i])) {
                array[count++] = array[i];
            }
        }
        return Arrays.copyOf(array, count);
    }

    public static int[] checkingForPositiveDigitsOf_aNumber(int[] array) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            int number = array[i];
            int temp = 0;
            boolean odd = false;

            while (number != 0) {
                temp = number % 10;
                if (temp % 2 != 0) {
                    odd = true;
                    break;
                }
                number = number / 10;
            }
            if (!odd) {
                array[count++] = array[i];
                System.out.println(Arrays.toString(Arrays.copyOf(array, count)));
            }

        }
        return Arrays.copyOf(array, count);
    }

    public static boolean isDigitsSumEven(int number) {
        var sum = 0;
        while (number != 0) {
            sum += number % BASE;
            number /= BASE;
        }
        return Utils.isEven(sum);
    }

    public static int[] isPalindrome(int[] array) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            int num = array[i];
            int rev = 0;
            int temp = 0;
            int originalNum = num;

            while (num != 0) {
                rev = num % 10;
                temp = temp * 10 + rev;
                num = num / 10;
            }

            if (originalNum == temp) {
                array[count++] = array[i];

            }

        }
        return Arrays.copyOf(array, count);
    }

}
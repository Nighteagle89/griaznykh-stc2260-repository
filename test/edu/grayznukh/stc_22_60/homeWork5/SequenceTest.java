package edu.grayznukh.stc_22_60.homeWork5;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class SequenceTest {
    private static Stream<Arguments> supplyParametersForConditionIsEven() {
        return Stream.of(
                Arguments.of(new int[]{253, 5_569, 1_567, 25, 0, -4_586}, new int[]{0, -4_586}),
                Arguments.of(new int[]{-168, 9_862, 0, 285, -9_485}, new int[]{-168, 9_862, 0}),
                Arguments.of(new int[]{0, 2_589, -52_983, 21, 4_586}, new int[]{0, 4_586})
        );

    }

    private static Stream<Arguments> supplyParametersForConditionIsDigitsSumEven() {
        return Stream.of(
                Arguments.of(new int[]{253, 5_569, 1_567, 25, 0, -4_586}, new int[]{253, 0}),
                Arguments.of(new int[]{-168, 9_862, 0, 285, -9_485}, new int[]{0, -9_485}),
                Arguments.of(new int[]{0, 2_589, -52_983, 21, 4_586}, new int[]{0, 2_589})
        );
    }

    private static Stream<Arguments> supplyParametersForConditionIsCheckingForPositiveDigitsOf_aNumber() {
        return Stream.of(
                Arguments.of(new int[]{253, 5_569, 1_567, 25, 0, -4_586}, new int[]{0}),
                Arguments.of(new int[]{-268, 9_862, 0, 285, -9_485}, new int[]{-268, 0}),
                Arguments.of(new int[]{0, 2_589, -52_983, 21, 4_286}, new int[]{0, 4_286})
        );
    }

    private static Stream<Arguments> supplyParametersForConditionIsDigitsPalindrome() {
        return Stream.of(
                Arguments.of(new int[]{253, 5_569, 1_567, 25, 0, -4_586, 1_225_221}, new int[]{0, 1_225_221}),
                Arguments.of(new int[]{-168, 9_862, 161, 285, -9_485}, new int[]{161}),
                Arguments.of(new int[]{0, 2_589, -52_983, 121, 4_586}, new int[]{0, 121})
        );
    }

    @ParameterizedTest
    @MethodSource("supplyParametersForConditionIsEven")
    @DisplayName("проверка на четность элементов массива")
    void filterByConditionIsEven(int[] array, int[] expected) {
        int[] actual = Sequence.filter(array, Utils::isEven);
        assertArrayEquals(expected, actual,
                String.format("Массивы не равны:%n\tожидалось %s, получилось %s%n",
                        Arrays.toString(expected), Arrays.toString(actual)));
    }


    @ParameterizedTest
    @MethodSource("supplyParametersForConditionIsDigitsSumEven")
    @DisplayName("Проверка четности суммы цифр элементов массива")
    void filterByConditionIsDigitsSumEven(int[] array, int[] expected) {
        int[] actual = Sequence.filter(array, Sequence::isDigitsSumEven);
        assertArrayEquals(expected, actual,
                String.format("Массивы не равны:%n\tожидалось %s, получилось %s%n",
                        Arrays.toString(expected), Arrays.toString(actual)));
    }

    @ParameterizedTest
    @MethodSource("supplyParametersForConditionIsCheckingForPositiveDigitsOf_aNumber")
    @DisplayName("Проверка четности цифр элементов массива")
    void filterByConditionIsCheckingForPositiveDigitsOf_aNumber(int[] array, int[] expected) {
        int[] actual = Sequence.checkingForPositiveDigitsOf_aNumber(array);
        assertArrayEquals(expected, actual,
                String.format("Массивы не равны:%n\tожидалось %s, получилось %s%n",
                        Arrays.toString(expected), Arrays.toString(actual)));
    }

    @ParameterizedTest
    @MethodSource("supplyParametersForConditionIsDigitsPalindrome")
    @DisplayName("Проверка цифр элементов массива на палиндромность")
    void filterByConditionIsDigitsPalindrome(int[] array, int[] expected) {
        int[] actual = Sequence.isPalindrome(array);
        assertArrayEquals(expected, actual,
                String.format("Массивы не равны:%n\tожидалось %s, получилось %s%n",
                        Arrays.toString(expected), Arrays.toString(actual)));
    }

}


